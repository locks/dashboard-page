import Component from "@glimmer/component";

export default class DashboardPage extends Component {
    didInsertElement() {
        let spinner = document.querySelector('.sk-cube-grid') as HTMLElement;
        spinner.hidden = true;
    }
}
