'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'dashboard-page',
    environment: environment
  };

  return ENV;
};
